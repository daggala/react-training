import React from 'react';

function Picture (props) {
  return(
      <img src={props.children} height={props.height}/>
);}

Picture.propTypes = {
  children: React.PropTypes.node,
  height: React.PropTypes.number,
};

export default Picture;
