import React from 'react';

import { assert } from 'chai';
import { describe, it } from 'mocha';
import { render } from 'enzyme';

import Description from './index';

describe('Description', () => {
  it('should render a <h1> and its children', () => {
     const wrapper = render(<Description>Hello world</Description>);
     const h1Element = wrapper.find('h1');
     assert.isOk(h1Element.length, 'H1 element not found');
     assert.strictEqual(h1Element.text(), 'Hello world',
       'child contents not found');
   });
});
