import React from 'react';

function Description ({children}) {
  return(
      <p>
        {...children}
      </p>
  );
}


Description.propTypes = {
  children: React.PropTypes.node,
  length: React.PropTypes.number,
};

export default Description;
