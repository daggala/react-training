import React from 'react';
import {describe, it }  from 'mocha';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Mission from './index';

const mock2Missions = [{
  id: 1,
  title: 'wonderfull',
  description: 'really wonderfull',
  src: '/dummy/link/to/my/image.jpg',
},{
  id: 2,
  title: 'wonderfull 2',
  description: 'really wonderfull2',
  src: '/dummy/link/to/my/image2.jpg',
}];

const mock3Missions = [{
  id: 1,
  title: 'wonderfull',
  description: 'really wonderfull',
  src: '/dummy/link/to/my/image.jpg',
},{
  id: 2,
  title: 'wonderfull 2',
  description: 'really wonderfull2',
  src: '/dummy/link/to/my/image2.jpg',
},{
  id: 3,
  title: 'wonderfull 3',
  description: 'really wonderfull3',
  src: '/dummy/link/to/my/image3.jpg',
}];


describe('<Mission />', () => {

  it(' should display a <Link>, <Description>  ', () => {
    const wrapper = shallow(<Mission list={mock2Missions}></Mission>);
    //It isn't really checking if it exists, try typing a typo, 'Lnk' or 'Dscription'
    expect(wrapper.find('Link')).to.exist;
    expect(wrapper.find('Description')).to.exist;
  });
  it(' should display 2 mission objects', () => {
    const wrapper = shallow(<Mission list={mock2Missions}></Mission>);
    expect(wrapper.find('Title')).to.have.length(2);
  });
  it(' should display 3 mission objects', () => {
    const wrapper = shallow(<Mission list={mock3Missions}></Mission>);
    expect(wrapper.find('Title')).to.have.length(3);
  });

});
