import React from 'react';
import Title from '../title';
import Description from '../description';
import Picture from '../picture';
import {Link} from 'react-router';


function Mission ({children}) {
  return(
    <div>
      {children.map(function(a){
        return (
          <div>
            <Link to={`/mission/${a.id}`}><Title>{a.title}</Title></Link>
            <Description length={35}>{a.description}</Description>
            <Picture children={a.image} height={150}></Picture>
          </div>
        );
      })}
    </div>
  );
}

Mission.propTypes = {
  children: React.PropTypes.array,
};

export default Mission;
