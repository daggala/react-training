import React from 'react';

import { assert } from 'chai';
import { describe, it } from 'mocha';
import { render } from 'enzyme';

import Title from './index';

describe('Title', () => {
  it('should render a <h1> and its children', () => {
     const wrapper = render(<Title>Hello world</Title>);
     const h1Element = wrapper.find('h1');
     assert.isOk(h1Element.length, 'H1 element not found');
     assert.strictEqual(h1Element.text(), 'Hello world',
       'child contents not found');
   });
});
