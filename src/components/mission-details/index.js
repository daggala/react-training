
import React from 'react';

import Title from '../title';
import Description from '../description';
import Picture from '../picture';

import missions from '../../assets/missions.json';

function MissionDetails (props) {

  //-1 because the first mission is stored as the 0th value in the object
  const mission = missions.missions[props.params.missionId - 1];

  return(
        <div>
          <Title>{mission.title}</Title>
          <Description length={1000}>{mission.description}</Description>
          <Picture children={mission.image} height={150}></Picture>
        </div>
    );
}

MissionDetails.propTypes = {
  props : React.PropTypes.object,
};

export default MissionDetails;
