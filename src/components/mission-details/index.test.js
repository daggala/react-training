import React from 'react';
import { describe, it, before, after} from 'mocha';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import fs from 'fs';
import MissionDetails from './index';
//import missions from '../../assets/missions.json';

const mock2Missions = [{
  id: 1,
  title: 'wonderfull',
  description: 'really wonderfull',
  src: '/dummy/link/to/my/image.jpg',
},{
  id: 2,
  title: 'wonderfull 2',
  description: 'really wonderfull2',
  src: '/dummy/link/to/my/image2.jpg',
}];

const missionURL =  {
  'params':{'missionId':'1'},
};

let saveMissionsJson;

describe('<MissionDetails />', () => {
  before('prepare json for tests', () =>{
    saveMissionsJson = fs.readFileSync('./src/assets/missions.json', 'utf-8');
    fs.writeFileSync('./src/assets/missions.json', JSON.stringify(mock2Missions));
  });
  // I keep getting the error: Can't read missonId of undefined.


  it('should render the elements <Title>, <Description> and <Picture>', () => {
    const wrapper = shallow(<MissionDetails missionId={missionURL}/>);
    expect(wrapper.find('Title')).to.have.length(1);
    expect(wrapper.find('Description')).to.have.length(1);
    expect(wrapper.find('Picture')).to.have.length(1);
  });

  it('should render a Description element with text < 1000', () => {
    const wrapper = shallow(<MissionDetails missionId={missionURL}/>);
    expect(wrapper.find('Description').text().length).to.be.below(1000);
  });

  //Probably too hard coded tests?
  it('should render a Picture element of size 350', () => {
    const wrapper = shallow(<MissionDetails missionId={missionURL}/>);
    expect(wrapper.find('Picture').props().height).to.equal(350);
  });
  after('restore json after tests', () =>{
    fs.writeFileSync('./src/assets/missions.json', saveMissionsJson);
  });

});
