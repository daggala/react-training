import React from 'react';

import Mission from '../mission';
import missions from '../../assets/missions.json';

function MissionsPage ({numberOfMissions}) {

  //const no = numberOfMissions;

  return(
      <div>
          <Mission>
              {missions.missions.slice(0,numberOfMissions)}
          </Mission>
      </div>
  );
}

MissionsPage.propTypes = {
  numberOfMissions: React.PropTypes.number,
};

export default MissionsPage;
