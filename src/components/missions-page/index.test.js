import React from 'react';
import fs from 'fs';
import { describe, it, before, after } from 'mocha';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import MissionsPage from './index';

const mock2Missions = { missions: [{
  id: 1,
  title: 'wonderfull',
  description: 'really wonderfull',
  src: '/dummy/link/to/my/image.jpg',
},{
  id: 2,
  title: 'wonderfull 2',
  description: 'really wonderfull2',
  src: '/dummy/link/to/my/image2.jpg',
},{
  id: 3,
  title: 'wonderfull 2',
  description: 'really wonderfull2',
  src: '/dummy/link/to/my/image2.jpg',
},{
  id: 4,
  title: 'wonderfull 2',
  description: 'really wonderfull2',
  src: '/dummy/link/to/my/image2.jpg',
}]};

let saveMissionsJson;

//We don't have to include a mockup json here because we don't use it when we create this component.
//..we only use it in the Mission property. Right?
describe('<MissionsPage />', () => {
  before('prepare json for tests', () =>{
    saveMissionsJson = fs.readFileSync('./src/assets/missions.json', 'utf-8');
    fs.writeFileSync('./src/assets/missions.json', JSON.stringify(mock2Missions));
  });
  it('should render one Mission element', () => {
    const wrapper = shallow(<MissionsPage numberOfMissions={3}/>);
    expect(wrapper.find('Mission')).to.have.length(1);
  });
  it('should slice shorten the list of missions based on MissionsPage prop number', () => {
    const wrapper = shallow(<MissionsPage numberOfMissions={3}/>);
    expect(wrapper.find('Mission').props().list.length).to.equal(3);
  });
  after('restore json after tests', () =>{
    fs.writeFileSync('./src/assets/missions.json', saveMissionsJson);
  });
});
