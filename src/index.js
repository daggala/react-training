import React from 'react';
import ReactDOM from 'react-dom';
import MissionsPage from './components/missions-page';
import MissionDetails from './components/mission-details';
//import Mission from './components/mission';

import {Router, Route, hashHistory} from 'react-router';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={() => {return <MissionsPage numberOfMissions={2} />}}></Route>
    <Route path="/mission/:missionId" component={MissionDetails}></Route>
  </Router>,
  document.getElementById('main')
);

/*
ReactDOM.render(
  <MissionsPage amount={3}/>,
  document.getElementById('main')
);
*/

//<Route path="mission" component={MissionDetails}></Route>
